# SAST (Static Application Security Testing) Demo Project

## What is this?

A composite project for testing of GitLab [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) functionality.

This project has a small amount of code for several [supported languages and frameworks](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) to trigger a variety of SAST scanners and output detected "vulnerabilities".

This project can be used to demo SAST functionality and expected results, or it can act as the sand in a sandbox for testing SAST and CI job modifications.

## Usage

1. Import this project to your SaaS namespace or self-managed instance.
1. Trigger a pipeline.
1. :tada:

## Troubleshooting Tips

- If SAST job is failing and it's unclear why, **enable [debug logging](https://docs.gitlab.com/ee/user/application_security/sast/index.html#sast-debug-logging)**

    ```yaml
    variables:
      SECURE_LOG_LEVEL: "debug"
    ```
    
- If customizations were made to SAST jobs, run a pipeline using bare-minimum SAST defaults to verify that customizations are not causing or contributing to the problem.

    ```yaml
    include:
      - template: Security/SAST.gitlab-ci.yml
    ```

## Resources

- [SAST Documentation](https://docs.gitlab.com/ee/user/application_security/sast/index.html)
  - [Supported Languages and Frameworks](https://docs.gitlab.com/ee/user/application_security/sast/index.html#supported-languages-and-frameworks)
  - [Analyzer Settings](https://docs.gitlab.com/ee/user/application_security/sast/index.html#analyzer-settings)
  - [Vulnerability Filters](https://docs.gitlab.com/ee/user/application_security/sast/index.html#vulnerability-filters)
- [Default SAST Template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml)

## Contributing

This project i s licensed under MIT license and is accepting contributions.

If you have a proposed improvement, create an issue. Or better yet - make the improvement yourself and submit a merge request!
